# Dockered Jenkins with Ansible

__Requirement:__
    
* ansible installed

__How to run:__
```
ansible-playbook provision.yaml
```